
import { canvas, clearCanvas } from "./canvas,js";
import { Timer } from "./Timer.js";

const STATUS = {
    READY: 0,
    STARTED: 1,
    PAUSED: 2
}

STATUS.READY

export class Level {
    constructor(options) {
        this.size = options.size || [canvas.clientWidth, canvas.height];
        this.cameraPos = options.cameraPos || [0, this.size[1] - canvas.height];
        this.originalPos = [...this.cameraPos];
        this.objects = [];
        this.objectsOfType = {
            Rectangle: [],
            player: [],
            Box: [],
            Goal: []
        };
        this.addObjects(options.objects || []);
        this.player = null;
        this.timer = new Timer();
        this.timer.update = (deltaTime) => this.update(deltaTime);
        this.status = STATUS.READY;
        this.won = false;

    }

    addObjects(objects) {
        objects.forEach(obj => this.addObject(obj));
    }

    addObject(obj) {
        const type = obj.type;
        this.objects.push(obj);
        this.objectsOfType[type].push(obj);
        obj.level = this;
    }

    update(deltaTime) {
        clearCanvas();
        this.updateObjects(deltaTime);
        this.updateCamera();
        this.drawObjects();
        this.checkWin();
    }

    updateObjects(deltaTime) {
        this.objects.forEach(obj => obj.update(deltaTime));
    }

    updateCamera() { 
    this.cameraPos[0] = Math.max(
        0,
        Math.min(this.size[0] - canvas.width, this.right - canvas.width / 2)
    );
    this.cameraPos[1] = Math.max(
        0,
        Math.min(this.size[1] - canvas.height, this.top - canvas.height / 2)
    );
}

    drawObjects() {
        this.objects.forEach(obj => obj.draw());
    }

    checkWin() {

    }
}
